import random
import unittest

class RandomTest(unittest.TestCase):
	"""Test case utilisé pour tester les fonctions du module 'random'."""

	def setUp(self):
		"""Initialisation des tests."""
		self.liste = list(range(10))

	def test_choice(self):
		"""Teste le fonctionnement de la fonction 'random.choice'."""
		elt = random.choice(self.liste)
		self.assertIn(elt, self.liste) #a in b ?

	def test_shuffle(self):
		"""Teste le fonctionnement de la fonction 'random.shuffle'."""
		random.shuffle(self.liste)
		self.liste.sort()
		self.assertEqual(self.liste, list(range(10)))  #a == b

	def test_sample(self):
		"""Teste le fonctionnement de la fonction 'random.sample'."""
		extrait = random.sample(self.liste, 5)
		for element in extrait:
			self.assertIn(element, self.liste)

	def test_sample_check_exception(self):
		"""Teste le fonctionnement de la fonction 'random.sample' dans 
			un cas d'appel incorrect
		"""
		with self.assertRaises(ValueError): #Vérifie que la fonction lève l'exception attendue
			random.sample(self.liste, 20)


	def test_sample_check_exception2(self):
		"""Teste le fonctionnement de la fonction 'random.sample' dans 
			un cas d'appel incorrect, seconde version
		"""
		self.assertRaises(ValueError, random.sample, self.liste, 20)


	def tearDown(self):
		"""Libération des ressources, fermeture des fichiers eventuellement ouverts dans setUp."""

	def test_choice_erroneous_test(self):
		"""Teste très mal le fonctionnement de la fonction 'random.choice'."""
		elt = random.choice(self.liste)
		self.assertIn(elt, ('a', 'b', 'c'))

	def test_crashing_test(self):
		"""Teste et crashe sur une division par zéro."""
		self.assertEqual(6, 6 / 0)

###############################################################
###################____QUESTION_____###########################
###############################################################
"""
###########################################################################
##########Combien de tests échouent, combien de tests crashent? Pourquoi?##
###########################################################################
test_choice_erroneous_test qui échoue (FAIL) et test_crashing_test qui crash (ERROR)

test_choice_erroneous_test :
test la fonction random.choice mais elle a besoin d'un int et la fonction assertIn est tester avec des str

test_crashing_test :
division par 0

################################################################################
##########Notez les deux façons de tester qu’un code lève une Exception#########
################################################################################

FAILED (failures=1, errors=1)
errors = le test crash  (par exemple de type fatal error) 
failures = Une erreur dans le test  car c'est pas ce qu'on demande mais ca ne crash pas
             
################################################################################
##########Cherchez sur internet le rôle des méthodes setUp et tearDown##########
################################################################################

Ces méthodes ont pour role d'ordaniser les tests dans le code.Elle sont appeler avant et aprés chaque méthode de type test.

SetUp indique que toute exception lévé (sauf AssertionError ou SkipTest) sera considèrer comme une erreur (ERROR) et non comme un test qui échoue (FAIL)
tearDown est appeler uniquement si setUp a été bien exécuté.Elle indique elle aussi une erreur mais une erreur suplémentaire et ne signale pas d'échec du test.
C'est le même principe que pour SetUp

Bien entendu, gitez-moi tout ça!

"""
