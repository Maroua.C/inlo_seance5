#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 14:22:41 2020

@author: c19012324
"""

from pile_file_OO_a_completer import Pile 
from pile_file_OO_a_completer import File

import unittest
from copy import deepcopy,copy


class Testpile(unittest.TestCase):
    
    ##############
    ##QUESTION_1##
    ##############
    def test_pile_depiler_vide(self):
        """ on ne peut dépiler une pile vide """
        pile=Pile()
        if pile.estVide() is True: # c'est la pile qu'on vient de cree qu'on veut tester d ou pile en min et pas en maj
            self.assertRaises(IndexError, Pile.depiler,pile)
            # type d'erreur , donne la fonction a tester et le dernier c'est pour l'argument
            #ici on aura alors pile.depiler(pile <- c'est le self) ne pas mettre de parenthese a depiler 
            #car on ne veut pas appeler la fonction mais la tester 
            
    def test_pile_sommet_vide(self):
        """  on ne peut lire le sommet d’une pile vide """
        pile=Pile()
        if pile.estVide() is True:
            self.assertRaises(IndexError, Pile.sommet,pile)
            
    ##############
    ##QUESTION_2##
    ##############
    def test_pile_vide(self):
        """ une pile venant d’être créée est vide """

        pile=Pile()
        pile.estVide() 
        #fait assertTrue car si pas true on a une erreur 
        self.assertTrue(Pile.estVide,pile)
        
    def test_pile_empiler_vide(self):
        """ toute pile sur laquelle on vient d’empiler un élément est non vide  """
        
        pile=Pile()
        pile.empiler('e')
        #pour vérifier que le pile n'est pas vide on va regarder son sommet 
        self.assertTrue(Pile.sommet,pile)

    def test_pile_inchangee(self):
        """ une pile qui subit un empilement suivi d’un dépilement est inchangée """
        
        pile=Pile()
        #La pile est non vide comme ca quand on va empiler et dépiler elle ne sera pas vide et va renvoyer le meme sommet a
        pile.empiler('a')
        
        #pile2 est un clone de pile
        pile2=copy(pile)
        pile.empiler('e')
        pile.depiler()
        
        sommet1=pile.sommet()
        sommet2=pile2.sommet()
        
        self.assertEqual(sommet1, sommet2)


    def test_pile_sommet_empiler(self):
        """ Le sommet d’une pile sur laquelle on vient d’empiler un élément e est e """
        
        pile=Pile()
        pile.empiler('e')
        self.assertTrue(Pile.sommet, 'e')
   


class Testfile(unittest.TestCase):
     
    ##############
    ##QUESTION_3##
    ##############
    def test_file_vide_emfile(self):
        """ Si une file est vide, alors la tête d’une file sur laquelle on enfile un élément e est e """
        
        file=File()
        if file.estVide()  is True:
            file.enfiler('e')
            self.assertTrue(File.tete, 'e')
            
            

    def test_file_enfile_file(self):
        """ Si une file n’est pas vide, alors la tête d’une file sur laquelle on enfile un élément e 
            est identique à la tête de la file avant l’opération d’enfilement """
            #on teste que la tete est tjr la meme 
            
        file=File()
        file.enfiler('e')
        test_tete1 = file.tete()
        file.enfiler('d')
        test_tete2 = file.tete()

        self.assertEqual(test_tete1,test_tete2)
        
            
        
    def test_file_enfile_defile(self):
        """ Une file vide qui subit un enfilement suivi d’un défilement est toujours une file vide """
       
        file=File()
        file.enfiler('e')
        file.defiler()
        self.assertTrue(File.estVide,file)
        
    def test_file_inchangee(self):
        """ Sur une file non vide, un enfilement suivi d’un défilement est identique à un défilement suivi d’un enfilement. """
        
        file=File()
        file.enfiler('n')
        file.enfiler('p')
        #maintenant la file est non vide
        file_enfilement_first = copy(file)
        file_defilement_first = copy(file)
        
        file_enfilement_first.enfiler('e')
        file_enfilement_first.defiler()
        
        file_defilement_first.defiler()
        file_defilement_first.enfiler('e')
        
        test1=file_defilement_first.tete()
        test2=file_enfilement_first.tete()

        self.assertEqual(test1,test2)
        
        """
        pour tester l'affirmation on va crée deux file cloner de la file de depart donc on a trois file identique (sans problème de pointeurs car on a utiliser copy)
        pour une on va d'abord enfiler et l'autre d'abord défiler puis on va tester les tete de chacune de ses deux files 
        normalement les tetes seront les même car on est partie sur la MEME file au départ. (si les files avaient été différente les tête le serait aussi et donc faudra choisir
        un autre critère pour tester
        """
 


    

        
    





















